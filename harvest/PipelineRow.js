define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-style',
  'dojo/dom-class',
  'entryscape-commons/defaults',
  'entryscape-commons/gce/GCERow',
  'di18n/localize',
], (declare, lang, domStyle, domClass, defaults, GCERow, localize) => {
  const getUser = (mail) => {
    const es = defaults.get('entrystore');
    return es.getREST().get(`${es.getBaseURI()}_principals?entryname=${mail}`)
      .then((data) => {
        if (data.length > 0) {
          return es.getContextById('_principals').getEntryById(data[0]);
        }
        return false;
      });
  };

  const canRemoveUser = (userEntry, groupEntry) => {
    const pgs = userEntry.getParentGroups();
    const contained = pgs.indexOf(groupEntry.getURI()) !== -1;
    return contained && pgs.length === 1;
  };

  /**
   * This row class is only needed to correctly remove associated user
   * if the user doing the deletion is in the admin-group.
   */
  return declare([GCERow], {
    removeRow(p) {
      const row = this;
      return p.then(() => {
        row.list.getView().removeRow(row);
        row.destroy();
        const ue = defaults.get('userEntry');
        ue.setRefreshNeeded();
        ue.refresh();
      }, () => {
        defaults.get('dialogs').acknowledge(row.nlsGenericBundle[row.nlsRemoveFailedKey]);
      });
    },

    /**
     * Override of method in ToggleRow, to avoid new release of commons due to two things:
     * First, GCERow always setting isEnabled to true.
     * Second, ToggleRow does not support public toggle being disabled
     *
     * TODO Fix better support in next version of commons and simplify this code
     */
    setToggled(isEnabled, isPublic) {
      const md = this.entry.getMetadata();
      const isPSI = Boolean(md.findFirstValue(this.entry.getResourceURI(), 'foaf:page'));
      const isAdmin = Boolean(defaults.get('isAdmin'));
      const inAdminGroup = Boolean(defaults.get('inAdminGroup'));
      // Do no let non-admins unpublish / publish PSI organisations
      this.toggleEnabled = !isPSI || isAdmin || inAdminGroup;
      if (this.toggleEnabled) {
        domClass.remove(this.publicToggleNode, 'disabled');
      }
      this.isPublicToggle = isPublic;
      if (isPublic) {
        domStyle.set(this.publicNode, 'display', '');
        domStyle.set(this.protectedNode, 'display', 'none');
      } else {
        domStyle.set(this.publicNode, 'display', 'none');
        domStyle.set(this.protectedNode, 'display', '');
      }
    },

    action_remove() {
      const context = this.getContext();
      const md = this.entry.getMetadata();
      const ruri = this.entry.getResourceURI();
      const mailto = md.findFirstValue(ruri, 'foaf:mbox');
      const mail = mailto ? mailto.substr(7) : null;
      const dialogs = defaults.get('dialogs');
      const removeRow = lang.hitch(this, this.removeRow);
      const bundle = this.nlsSpecificBundle;

      const confirmRemoveGCU = () => {
        const message = localize(bundle, 'confirmRemovePipelineAndUser', mail);
        return dialogs.confirm(message, null, null);
      };
      const confirmRemoveGC = () => dialogs.confirm(bundle.confirmRemovePipeline, null, null);

      defaults.get('getGroupWithHomeContext')(this.getContext()).then((groupEntry) => {
        const removeGC = () => removeRow(context.getEntry()
          .then(hcEntry => hcEntry.del()).then(() => groupEntry.del()));
        const noop = () => {};
        const ui = defaults.get('userInfo');
        if (mail && ui.user !== mail && defaults.get('hasAdminRights')) {
          getUser(mail).then((userEntry) => {
            if (userEntry && canRemoveUser(userEntry, groupEntry)) {
              confirmRemoveGCU().then(() => {
                userEntry.del().then(removeGC);
              }, noop);
            } else {
              confirmRemoveGC().then(removeGC).then(() => {
                if (userEntry) {
                  userEntry.setRefreshNeeded();
                  userEntry.refresh();
                }
              }, noop);
            }
          });
        } else {
          confirmRemoveGC().then(removeGC, noop);
        }
      });
    },
  });
});
