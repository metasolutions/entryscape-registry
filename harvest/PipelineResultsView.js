define([
  'dojo/_base/declare',
  '../defaults',
  'store/types',
  'store/terms',
  './PipelineResult',
  'config',
  'di18n/localize',
  'rdforms/utils',
  'dijit/_WidgetsInTemplateMixin',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'entryscape-commons/components/common/Group',
  'entryscape-commons/components/common/Title',
  'entryscape-commons/components/common/keyvalue/KeyValueList',
  'entryscape-commons/components/common/panel/PanelGroup',
  'di18n/NLSMixin',
  'dojo/text!./PipelineResultsViewTemplate.html',
  'i18n!nls/esrePipelineResult',
], (
  declare, defaults, types, terms, PipelineResult, config, localize, utils,
  _WidgetsInTemplateMixin, _WidgetBase, _TemplatedMixin, GroupComponent, TitleComponent,
  KeyValueListComponent, PanelGroupComponent, NLSMixin, template,
) =>
  declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    templateString: template,
    maxWidth: 800,
    nlsBundles: ['esrePipelineResultListDialog'],
    jobEntries: [],
    latestJobEntry: null,
    bid: 'esrePipelineResultsView',
    inDialog: true,

    constructor(params) {
      if ('inDialog' in params) {
        this.inDialog = params.inDialog;
      }
      if (this.inDialog) {
        this.dialog = params.dialog;
      }
    },

    async show(params) {
      this.contextId = params.params.context;
      await this.localeReady;
      await this.loadJobEntries();
      this.renderViewLabel();
      await this.renderJobEntries();
      if (this.latestJobEntry) {
        // might be the case that no job entries exist yet
        this.isPSIOrg = this.latestJobEntry.getMetadata().find(
          null, 'dcterms:subject',
          { type: 'literal', value: 'psi' },
        ).length > 0;
      } else {
        this.isPSIOrg = (this.pipelineEntry.getMetadata()
          .findFirstValue(this.pipelineEntry.getResourceURI(), 'foaf:page') != null);
      }
      this.renderOrganizationInfo();
    },

    execute() {
      this.renderJobEntries(true); // just rerender to disabled the re-harvest button
      const es = defaults.get('entrystore');
      this.pipelineEntry.getResource().then((pipeline) => {
        pipeline.execute().then((results) => {
          if (results.length === 1) {
            es.getEntry(results[0]).then((resultEntry) => {
              this.jobEntries.splice(0, 0, resultEntry);
              this.renderJobEntries();
            });
          }
        });
      });
    },

    getViewLabel(view, params, callback) {
      this.viewCallback = callback;
      this.renderViewLabel();
    },

    renderViewLabel() {
      if (this.viewCallback && this.latestJobEntry) {
        const rdfutils = defaults.get('rdfutils');
        const name = rdfutils.getLabel(this.latestJobEntry) || '-';
        this.viewCallback(name, name);
        delete this.viewCallback;
      }
    },

    async renderOrganizationInfo() {
      const data = {};
      const pipelineResource = await this.pipelineEntry.getResource();
      if (this.isPSIOrg) {
        data[this.NLSBundle0.orgId] = this.pipelineEntry.getMetadata()
          .findFirstValue(null, 'dcterms:identifier') || this.NLSBundle0.notFound;
        data[this.NLSBundle0.psiPage] = pipelineResource.getTransformProperty('check', 'source')
          || this.NLSBundle0.notFound;
      }
      data[this.NLSBundle0.dcatAPI] = pipelineResource.getTransformProperty('fetch', 'source')
        || this.NLSBundle0.notFound;

      // userName
      const mdEntry = this.pipelineEntry.getMetadata();
      const ruri = this.pipelineEntry.getResourceURI();
      const title = mdEntry.findFirstValue(null, 'dcterms:title');
      const name = mdEntry.findFirstValue(null, 'foaf:mbox');
      const psiTag = mdEntry.find(
        ruri, 'dcterms:subject',
        { type: 'literal', value: 'psi' },
      ).length > 0;
      if (psiTag && name) {
        data[this.NLSBundle0.userId] = name.substr(7);
        // TODO
        // domStyle.set(this.__footer, 'display', 'block');
      }
      let button = {};

      // await for the contexts entry in order to get a boolean out of isPublic
      await this.pipelineEntry.getContext().getEntry();

      if (this.inDialog && this.pipelineEntry.isPublic()) {
        const text = this.NLSBundle0.openSeparateReportWindow;
        const popover = this.NLSBundle0.openSeparateReportWindowTitle;
        const icon = 'fa-external-link';
        const sm = defaults.get('siteManager');
        button = {
          element: 'a',
          text,
          href: sm.getViewPath('harvest__org', { context: this.contextId }),
          target: '_blank',
          externalLink: true,
          icon,
          popover,
          classNames: ['btn-default'],
        };
      }

      const contactText = config.registry && config.registry.contactText ?
        utils.getLocalizedValue(config.registry.contactText).value : undefined;

      // groups and renders renders a title with a key-value component
      this.__organizationInfo.style.display = '';
      m.render(this.__organizationInfo, [m(GroupComponent, {
        components: [
          m(TitleComponent, { title, hx: 'h3', button }),
          m(KeyValueListComponent, { data }),
        ],
      }),
        contactText ?
        m('div', { class: `alert alert-info ${this.bid}__info` }, [
          m('i', { class: `fa fa-info-circle fa-2x ${this.bid}__infoIcon` }),
          m('span', {}, contactText),
        ]) : null,
      ]);
    },

    async loadJobEntries() {
      this.jobEntries = [];
      const es = defaults.get('entrystore');
      await es.newSolrQuery()
        .graphType(types.GT_PIPELINERESULT)
        .context(this.contextId)
        .sort('created+asc')
        .forEach((entry) => {
          this.jobEntries.push(entry);
        });
      [this.latestJobEntry] = this.jobEntries;
      await es.newSolrQuery()
        .tagLiteral('opendata')
        .context(this.contextId)
        .graphType(types.GT_PIPELINE)
        .list()
        .getEntries(0)
        .then((entries) => {
          if (entries.length > 0) {
            [this.pipelineEntry] = entries;
          }
        });
    },

    async renderJobEntries(onlyLockButton = false) {
      const readyLocalesPromises = this.jobEntries.map((entry) => {
        const args = this.dialog ? { entry, currentDialog: this.dialog } : { entry };
        const pr = new PipelineResult(args);
        return [pr.localeReady, pr];
      });

      // just wait until all locales are ready
      const localPromises = readyLocalesPromises.map(readyLocalesPromise => readyLocalesPromise[0]);
      await Promise.all(localPromises);

      const pipeResultsPromises = readyLocalesPromises.map(readyLocalesPromise => readyLocalesPromise[1]);
      const pipeResults = pipeResultsPromises.map(pipelineResult => pipelineResult.getData());

      const title = localize(this.NLSBundle0, 'harvestingLatestX', pipeResults.length);
      let button = {};
      if (this.pipelineEntry && this.pipelineEntry.canWriteResource()) {
        const text = this.NLSBundle0.pRcreateButtonLabel;
        const popover = this.NLSBundle0.pRcreatePopoverTitle;
        let onclick = this.execute.bind(this);
        let disabled = onlyLockButton;
        if (this.jobEntries.length > 0) {
          const status = this.jobEntries[0].getEntryInfo().getStatus();
          // disables the button if method is caleed with onlyLockButton = true or status is
          // Pending/Progress
          disabled = onlyLockButton || (status === terms.status.InProgress || status === terms.status.Pending);
          onclick = disabled ? undefined : onclick;
        }
        button = {
          text,
          popover,
          onclick,
          disabled,
          classNames: ['btn-raised', 'btn-success'],
        };
      }
      m.render(this.__pipelineResultList, m(PanelGroupComponent, {
        panels: pipeResults,
        title,
        button,
      }));
    },
  }));
