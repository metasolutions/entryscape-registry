define([
  'dojo/_base/declare',
  'entryscape-commons/list/common/ListDialogMixin',
  './PipelineResultsView',
  'di18n/NLSMixin',
  'entryscape-commons/dialog/TitleDialog',
  'i18n!nls/esrePipelineResultListDialog',
], (declare, ListDialogMixin, PipelineResultsView, NLSMixin, TitleDialog) =>
  declare([TitleDialog, ListDialogMixin, NLSMixin.Dijit], {
    maxWidth: 800,
    nlsBundles: ['esrePipelineResultListDialog'],
    nlsHeaderTitle: 'pipelineResultRowHeader',
    nlsFooterButtonLabel: 'pipelineResultRowButton',

    postCreate() {
      this.inherited(arguments);
      this.plist = new PipelineResultsView({
        dialog: this,
      }, this.containerNode);
    },
    localeChange() {
      this.updateLocaleStrings(this.NLSBundle0);
    },
    open(params) {
      this.inherited(arguments);
      this.entry = params.row.entry;
      this.plist.show({ params: { context: this.entry.getContext().getId() } });
      this.show();
    },
  }));
