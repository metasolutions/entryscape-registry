define([
  'mithril',
  'entryscape-commons/components/common/form/Input',
  'jquery',
], (m, Input, jquery) => {
  const Checkbox = {
    onupdate(vnode) {
      if (vnode.attrs.tooltip) {
        const label = jquery(vnode.dom).find('label')[0];
        jquery(label).tooltip();
      }
    },
    view(vnode) {
      const {
        type = 'togglebutton',
        label,
        tooltip,
        input,
      } = vnode.attrs;

      return m('.form-group', [
        m(`div.${type}`, [
          m('label', {
            'data-toggle': 'tooltip',
            'data-placement': 'right',
            'data-original-title': tooltip,
          }, [
            m(Input, { input }),
            m('span', label),
          ]),
        ]),
      ]);
    },
  };

  return Checkbox;
});

