define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'entryscape-commons/gce/List',
  'store/types',
  './PipelineResultsViewDialog',
  './CreatePipelineDialog',
  './EditPipelineDialog',
  './PipelineRow',
  'i18n!nls/esreHarvest',
  'i18n!nls/escoList',
], (
  declare, defaults, GCEList, types, PipelineResultsViewDialog, CreatePipelineDialog,
  EditPipelineDialog, PipelineRow,
) => {
  const ns = defaults.get('namespaces');
  return declare([GCEList], {
    includeCreateButton: true,
    includeInfoButton: false,
    includeEditButton: false,
    includeRemoveButton: true,
    nlsGCEPublicTitle: 'publicHarvestTitle',
    nlsGCEProtectedTitle: 'privateHarvestTitle',
    nlsBundles: ['escoList', 'esreHarvest'],
    entryType: ns.expand('store:Pipeline'),
    class: 'pipeline',
    rowClass: PipelineRow,
    rowClickDialog: 'status',
    rowActionNames: ['status', 'configure', 'remove'],
    includeSortOptions: true,
    useNoLangSort: true,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('create', CreatePipelineDialog);
      this.registerDialog('status', PipelineResultsViewDialog);
      this.registerDialog('configure', EditPipelineDialog);
      this.registerRowAction({
        name: 'status',
        button: 'default',
        iconType: 'fa',
        icon: 'info-circle',
        nlsKey: 'harvestingJobs',
        nlsKeyTitle: 'harvestingJobsTitle',
      });
      this.registerRowAction({
        name: 'configure',
        button: 'default',
        iconType: 'fa',
        icon: 'cogs',
        nlsKey: 'configure',
        nlsKeyTitle: 'configureTitle',
      });
    },

    show() {
      const esu = defaults.get('entrystoreutil');
      esu.preloadEntries(ns.expand('store:Pipeline'), defaults.get('context'));
      this.render();
    },
    getEmptyListWarning() {
      return this.NLSBundle1.emptyListWarning;
    },
    getNlsForCButton() {
      return {
        nlsKey: this.NLSBundle1.cOHeader,
        nlsKeyTitle: this.NLSBundle1.cOHeader,
      };
    },
    /**
     *
     * @param params
     * @param row
     */
    installActionOrNot() {
    },
    getSearchObject() {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery().graphType(types.GT_PIPELINE).tagLiteral('opendata');
    },
  });
});
