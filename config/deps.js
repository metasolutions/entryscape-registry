require.config({
  baseUrl: '/libs', // Path relative to bootstrapping html file.
  paths: { // Paths relative baseUrl, only those that deviate from baseUrl/{modulename} are
    // explicitly listed.
    'entryscape-registry': '..',
    templates: 'rdforms-templates',
    nls: '../nls/merged',
    theme: '../theme',
    text: 'requirejs-text/text',
    i18n: 'di18n/i18n',
    fuelux: 'fuelux/js',
    bootstrap: 'bootstrap-amd/lib',
    bmd: 'bmd/dist',
    bmddtp: 'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker',
    selectize: 'selectize/js/selectize',
    select2: 'select2/src/js',
    jquery: 'jquery/src',
    sizzle: 'sizzle/dist/sizzle',
    'jquery.mousewheel': 'select2/src/js/jquery.mousewheel.shim',
    requireLib: 'requirejs/require',
    md5: 'md5/js/md5.min',
    typeahead: 'typeahead.js/dist/typeahead.jquery',
    jstree: 'jstree/src/jstree',
    'jstree.dnd': 'jstree/src/jstree.dnd',
    'jstree.checkbox': 'jstree/src/jstree.checkbox',
    'jstree.wholerow': 'jstree/src/jstree.wholerow',
    leaflet: 'leaflet/dist/leaflet',
    Chartist: 'chartist/dist/chartist.min',
    'chartist-plugin-legend': 'chartist-plugin-legend-latest/chartist-plugin-legend',
  },
  packages: [ // Config defined using packages to allow for main.js when requiring just config.
    {
      name: 'config',
      location: '../config',
      main: 'main',
    },
    {
      name: 'moment',
      main: 'moment',
    },
    {
      name: 'mithriljs',
      main: 'mithril',
    },
    {
      name: 'babel-polyfill',
      location: 'babel-polyfill/dist',
      main: 'polyfill',
    },
    {
      name: 'formdata-polyfill',
      location: 'formdata-polyfill',
      main: 'formdata.min.js',
    },
    {
      name: 'blob',
      main: 'Blob',
    },
    {
      name: 'FileSaver',
      location: 'filesaver',
      main: 'FileSaver',
    },
  ],
  shim: {
    typeahead: { deps: ['jquery'] },
    FileSaver: { deps: ['entryscape-registry/shim/blob'] },
  },
  map: {
    '*': {
      mithril: 'entryscape-commons/shim/mithril',
      chartist: 'Chartist',
      polyfill: 'entryscape-commons/shim/polyfill',
      jquery: 'jquery/jquery', // In general, use the main module (for all unqualified jquery dependencies).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
      has: 'dojo/has', // Use dojos has module since it is more clever.
      'dojo/text': 'text', // Use require.js text module
      // Make sure i18n, dojo/i18n and di18n/i18n are all treated as a SINGLE module named i18n.
      // (We have mapped i18n to be the module provided in di18n/i18n, see paths above.)
      'dojo/i18n': 'i18n',
      'di18n/i18n': 'i18n',
      'rdforms/model/Engine': 'rdforms/model/engine',
    },
    jquery: {
      jquery: 'jquery', // Reset (override general mapping) to normal path (jquerys has dependencies to specific modules).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
      'external/sizzle/dist/sizzle': 'sizzle',
    },
    bootstrap: {
      jquery: 'jquery', // Reset (override general mapping) to normal path (bootstraps has dependencies to specific dependencies).
      'jquery/selector': 'jquery/selector-sizzle', // Always use the jquery sizzle selector engine.
    },
    'store/Rest': {
      'dojo/request': 'dojo/request/xhr', // Force using xhr since we know we are in the browser
      'dojo/request/script': 'dojo/request/script',
      'dojo/request/iframe': 'dojo/request/iframe', // Override above line for iframe path.
    },
    'rdforms/template/bundleLoader': {
      'dojo/request': 'dojo/request/xhr', // Force using xhr since we know we are in the browser
    },
  },
  deps: [
    'polyfill',
    'formdata-polyfill',
    'moment/locale/nb',
    'moment/locale/sv',
    'moment/locale/da',
    'moment/locale/de',
    'entryscape-commons/commonDeps',
    'entryscape-commons/nav/Cards',
    'entryscape-commons/gce/Cards',
    'entryscape-catalog/catalog/List',
    'entryscape-catalog/files/List',
    'entryscape-catalog/datasets/List',
    'entryscape-catalog/responsibles/List',
    'entryscape-catalog/candidates/CandidateList',
    'entryscape-catalog/results/ResultsList',
    'entryscape-admin/contexts/List',
    'entryscape-admin/groups/List',
    'entryscape-admin/users/List',
    'entryscape-catalog/public/Public',
    'entryscape-catalog/search/Search',
    'entryscape-catalog/search/DatasetSearch',
    'entryscape-commons/rdforms/GeonamesChooser',
    'entryscape-commons/rdforms/SkosChooser',
    'entryscape-commons/rdforms/EntryChooser',
    'entryscape-registry/source/Source',
    'entryscape-registry/merge/Merge',
    'entryscape-registry/present/List',
    'entryscape-registry/validate/Report',
    'entryscape-registry/harvest/List',
    'entryscape-registry/harvest/PipelineResultsView',
    'entryscape-registry/status/PSIStatus',
    'entryscape-registry/status/OtherStatus',
    'entryscape-registry/status/Visualization',
//    'entryscape-registry/faq/Faq',
    'entryscape-registry/nav/Start',
    'entryscape-registry/datasets/Datasets',
    'entryscape-registry/convert/Convert',
    'templates/skos/skos',
    'templates/dcterms/dcterms',
    'templates/foaf/foaf',
    'templates/vcard/vcard',
    'templates/odrs/odrs',
    'templates/dcat-ap/dcat-ap_props',
    'templates/dcat-ap/dcat-ap',
    'templates/entryscape/esc',
    'select2/select2/i18n/sv', // Explicit load of swedish language for select2 (no require-nls support)
    'entryscape-commons/bmd/all',
    'dojo/request/script',
  ],
});
