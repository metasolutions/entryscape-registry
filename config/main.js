define([
  'entryscape-commons/merge',
  'entryscape-admin/config/adminConfig',
  'entryscape-catalog/config/catalogConfig',
  'entryscape-workbench/config/workbenchConfig',
], (merge, adminConfig, catalogConfig, workbenchConfig) =>
  merge(adminConfig, catalogConfig, workbenchConfig, {
    theme: {
      appName: 'Registry',
      showModuleNameInHeader: true,
      startBanner: {
        header: {
          en: 'EntryScape Registry',
          sv: 'EntryScape Registry',
          de: 'Register offener Verwaltungsdaten',
        },
        text: {
          en: 'EntryScape Registry is a supplement to an open data portal. Here are tools that are helpful for organizations that want to make available its open data.',
          sv: 'EntryScape Registry är ett komplement till en öppen dataportal. Här finns verktyg som är till hjälp för organisationer som vill tillgängliggöra sina öppna data.',
          de: 'Hier finden Sie die Werkzeuge für Behörden, die Daten der öffentlichen Verwaltung zur Weiterverwendung durch Dritte bereitstellen wollen.',
        },
        icon: '',
        details: {
          buttonLabel: { en: 'Get started', sv: 'Kom igång', de: 'Anfangen' },
          header: { en: 'Getting started guide', sv: 'Kom-igång guide', de: 'Erste Schritte Anleitung' },
          path: '',
        },
      },
    },
    locale: {
      fallback: 'en',
      supported: [
        {
          lang: 'de', flag: 'de', label: 'Deutsch', labelEn: 'German',
        },
        {
          lang: 'en', flag: 'gb', label: 'English', labelEn: 'English',
        },
        {
          lang: 'sv', flag: 'se', label: 'Svenska', labelEn: 'Swedish',
        },
      ],
    },
    itemstore: {
      bundles: [
//        'entryscape-registry/templates/faq',
      ],
    },
    registry: {
      validationProfiles: [
        { name: 'dcat_ap_se', label: { en: 'Swedish DCAT-AP profile' } },
        { name: 'dcat_ap_dk', label: { en: 'Danish DCAT-AP profile' } },
      ],
      validationTypes: [
        'dcat:Catalog',
        'dcat:Dataset',
        'dcat:Distribution',
        'vcard:Kind',
        'vcard:Individual',
        'vcard:Organization',
        'foaf:Agent'],
      mandatoryValidationTypes: [
        'dcat:Catalog',
        'dcat:Dataset',
      ],
      recipes: ['DCAT', 'INSPIRE', 'CKAN'],
    },
    catalog: {
      catalogLimit: 1,
      datasetLimit: 3,
      fileuploadDistribution: false,
      catalogCollaboration: false,
      excludeEmptyCatalogsInSearch: true,
    },
    site: {
      siteClass: 'entryscape-commons/nav/Site',
      controlClass: 'entryscape-commons/nav/Layout',
      signinView: 'signin',
      permissionView: 'permission',
      startView: 'start',
      sidebar: { wide: false, always: true, replaceTabs: true },
      moduleList: ['status', 'search', 'register', 'toolkit', 'admin'],
      '!modules': {
        status: {
          productName: 'Status',
          startView: 'status__visualization', // compulsory
          title: { en: 'Status report', sv: 'Statusrapport', de: 'Statusbericht' },
          sidebar: true,
          asCrumb: true,
          faClass: 'eye',
          text: {
            sv: 'En automatiserad granskning av vilka offentliga organisationer som redovisar sin öppna data',
            en: 'An automated audit of which public organizations that document their open data',
            de: 'Ein automatisch erstellter Bericht über welche öffentlichen Organisationen ihre offenen Daten dokumentieren',
          },
        },
        search: {
          productName: 'Browse',
          startView: 'catalog__search', // compulsory
          title: { en: 'Dataset search', sv: 'Datamängds&shy;sök', de: 'Datensatz&shy;suche' },
          faClass: 'search',
          sidebar: true,
          text: {
            sv: 'Sök fram och utforska de datamängder som framgångsrikt skördats',
            en: 'Search and view the datasets that has been successfully harvested',
            de: 'Suchen und anzeigen von erfolgreich geharvesteten Datensätzen',
          },
        },
        register: {
          productName: 'Organizations',
          title: {
            en: 'Organizations',
            sv: 'Organisationer',
            de: 'Quellen',
          },
          startView: 'harvest__list', // compulsory,
          asCrumb: true,
          faClass: 'list',
          sidebar: true,
          text: {
            sv: 'Registera organisationer och deras kataloger som ska skördas',
            en: 'Register organizations and their catalogs to be harvested',
            de: 'Registrieren von Katalogen die geharvested werden sollen',
          },
        },
        toolkit: {
          faClass: 'wrench',
          title: { en: 'Toolkit', sv: 'Verktygslåda', de: 'Toolkit' },
          startView: 'toolkit__rdf__source', // compulsory,
          asCrumb: true,
          sidebar: true,
          text: {
            sv: 'En verktygslåda för att jobba med DCAT-AP metadata',
            en: 'A toolkit for working with DCAT-AP metadata',
            de: 'Ein Toolkit für die Arbeit mit DCAT-AP Metadaten',
          },
        },
        admin: {
          title: { en: 'Admini&shy;stration', sv: 'Admini&shy;strera', de: 'Verwaltung' },
          faClass: 'cogs',
          startView: 'admin__users',
          sidebar: true,
          restrictTo: 'admin',
          text: {
            sv: 'Administrera projekt, användare och grupper',
            en: 'Manage projects, users and groups',
            de: 'Verwaltung von Projekten, Benutzern und Gruppen',
          },
        },
      },
      views: {
        start: {
          class: 'entryscape-registry/nav/Start',
          title: {
            en: 'Start', sv: 'Start', da: 'Start', de: 'Start',
          },
          route: '/start',
        },
        signin: {
          title: {
            en: 'Sign in/out', sv: 'Logga in/ut', da: 'Login/,d', de: 'An-/Abmelden',
          },
          class: 'entryscape-commons/nav/Signin',
          constructorParams: { nextView: 'start' },
          route: '/signin',
        },
        permission: {
          name: 'permission',
          title: {
            en: 'You do not have permission to view this page',
            sv: 'Logga in/ut',
            da: 'Login/ud',
            de: 'An-/Abmelden',
          },
          class: 'entryscape-commons/nav/Permission',
          route: '/permission',
        },
        harvest__list: {
          class: 'entryscape-registry/harvest/List',
          title: { en: 'Harvesting sources', sv: 'Skördningskällor', de: 'Harvesting-Quellen' },
          hidden: true,
          faClass: 'list',
          route: '/organization',
          module: 'register',
        },
        harvest__org: {
          class: 'entryscape-registry/harvest/PipelineResultsView',
          faClass: 'question',
          parent: 'harvest__list',
          route: '/organization/:context',
          module: 'register',
          constructorParams: { inDialog: false },
        },
        status__visualization: {
          class: 'entryscape-registry/status/Visualization',
          title: { en: 'Overview', sv: 'Översikt' },
          route: '/status/overview',
          module: 'status',
        },
        status__public: {
          class: 'entryscape-registry/status/PSIStatus',
          title: {
            en: 'Public organizations',
            sv: 'Offentliga organisationer',
            de: 'Öffentliche Organisationen',
          },
          route: '/status/public',
          module: 'status',
        },
        status__other: {
          class: 'entryscape-registry/status/OtherStatus',
          title: {
            en: 'Other organizations',
            sv: 'Övriga organisationer',
            de: 'Übrige Organisationen',
          },
          route: '/status/other',
          module: 'status',
        },
        toolkit__rdf__source: {
          class: 'entryscape-registry/source/Source',
          title: { en: 'Catalog source', sv: 'Katalog&shy;källa', de: 'Katalogquelle' },
          faClass: 'database',
          route: '/toolkit/source',
          module: 'toolkit',
        },
        toolkit__validator__report: {
          class: 'entryscape-registry/validate/Report',
          title: { en: 'Validate', sv: 'Validera', de: 'Validieren' },
          faClass: 'check-square-o',
          text: {
            sv: 'Validera dina datamängdsbeskrivningar',
            en: 'Validate your dataset descriptions',
            de: 'Validieren Sie Ihre Datensatz-Beschreibungen',
          },
          route: '/toolkit/validate',
          module: 'toolkit',
        },
        toolkit__dcat__merge: {
          class: 'entryscape-registry/merge/Merge',
          faClass: 'filter',
          title: { en: 'Merge', sv: 'Slå samman', de: 'Kombinieren' },
          route: '/toolkit/merge',
          module: 'toolkit',
        },
        toolkit__dcat__view: {
          class: 'entryscape-registry/present/List',
          faClass: 'search',
          title: { en: 'Explore', sv: 'Utforska', de: 'Untersuchen' },
          route: '/toolkit/view',
          module: 'toolkit',
        },
        toolkit__dcat__convert: {
          class: 'entryscape-registry/convert/Convert',
          faClass: 'random',
          title: { en: 'Convert', sv: 'Konver&shy;tera', de: 'Konvertieren' },
          route: '/toolkit/convert',
          module: 'toolkit',
        },
        catalog__datasets__preview: {
          route: '/search/:context/dataset/:dataset',
          parent: 'catalog__search',
          module: 'search',
        },
      },
    },
    entitytypes: {
      question: {
        label: { en: 'Question' },
        rdfType: 'http://schema.org/Question',
        template: 'faq:Question',
        includeInternal: true,
        inlineCreation: true,
        split: true,
        publishable: true,
        faClass: 'question',
        contentviewers: [{
          name: 'answerview',
          relation: 'http://schema.org/acceptedAnswer',
          linkedEntityType: 'answer',
        }],
      },
      answer: {
        label: { en: 'Answer' },
        rdfType: 'http://schema.org/Answer',
        template: 'faq:Answer',
        includeInternal: true,
        inlineCreation: true,
        dependant: true,
      },
    },
    contentviewers: {
      answerview: {
        class: 'entryscape-commons/contentview/LinkedEntriesView',
        label: { en: 'Answer', sv: 'Answer' },
      },
    },
  }, __entryscape_config));
