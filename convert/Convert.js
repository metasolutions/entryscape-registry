define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-construct',
  './convertScript',
  'entryscape-commons/defaults',
  'entryscape-commons/view/PublicView',
  'rdfjson/namespaces',
  'di18n/NLSMixin',
  'di18n/localize',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./ConvertTemplate.html',
  'i18n!nls/esreConvert',
  'i18n!nls/esreSource',
], (
  declare, domAttr, domConstruct, convert, defaults, PublicView, namespaces,
  NLSMixin, localize, _WidgetBase, _TemplatedMixin, template,
) =>
  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    bid: 'esreConvert',
    nlsBundles: ['esreConvert', 'esreSource'],
    templateString: template,

    __convert: null,
    __toBeConverted: null,

    postCreate() {
      this.inherited('postCreate', arguments);
    },

    checkConvert() {
      domAttr.set(this.__table, 'innerHTML', '');

      const report = convert(this._graph, true);

      if (report.count === 0) {
        domAttr.set(this.__convert, 'disabled', 'disabled');
      } else {
        domAttr.remove(this.__convert, 'disabled');
      }

      this.localeReady.then(() => {
        const b = this.NLSBundles.esreConvert;
        domAttr.set(this.__toBeConverted, 'innerHTML', localize(b, 'toBeConverted', report.count));
        Object.keys(report).forEach((prop) => {
          if (prop !== 'count') {
            if (report[prop].fixes && report[prop].fixes.length) {
              report[prop].fixes.forEach((fix) => {
                const tr = domConstruct.create('tr', null, this.__table);
                domConstruct.create('td', {
                  innerHTML: namespaces.shorten(fix.s),
                  title: fix.s,
                }, tr);
                const fixType = fix.t === 'p' ? b.predicateFix : b.objectFix;
                domConstruct.create('td', { innerHTML: fixType }, tr);
                domConstruct.create('td', {
                  innerHTML: namespaces.shorten(fix.from),
                  title: fix.from,
                }, tr);
                domConstruct.create(
                  'td',
                  { innerHTML: namespaces.shorten(fix.to), title: fix.to },
                  tr,
                );
              }, this);
            }
          }
        });
      });
    },

    convert() {
      convert(this._graph);
      this.checkConvert();
    },

    show() {
      const graph = defaults.get('clipboardGraph');
      if (graph == null || graph.isEmpty()) {
        this.localeReady.then(() => {
          const bundle = this.NLSBundles.esreSource;
          return defaults.get('dialogs').acknowledge(bundle.noRDF, bundle.noRDFProceed);
        }).then(() => {
          defaults.get('siteManager').render('toolkit__rdf__source');
        });
        return;
      }
      this._graph = graph;
      this.checkConvert(graph);
    },
  }));
