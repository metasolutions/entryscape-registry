/* global define */
define(['dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-class',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'di18n/NLSMixin',
  'di18n/localize',
  'entryscape-commons/defaults',
  'rdforms/view/ValidationPresenter',
  'dojo/text!./ClassReportTemplate.html',
  'dojo/text!./InstanceReportTemplate.html',
  'jquery',
  'bootstrap/collapse',
  'i18n!nls/esreReport',
], (
  declare, lang, array, domClass, domConstruct, domAttr, _WidgetBase,
  _TemplatedMixin, NLSMixin, localize, defaults, ValidationPresenter, ClassReportTemplate,
  InstanceReportTemplate, jquery,
) => {
  const ns = defaults.get('namespaces');
  const InstanceReport = declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    report: null,
    template: null,
    graph: null,
    nlsBundles: ['esreReport'],
    templateString: InstanceReportTemplate,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.localeReady.then(() => {
        const messages = this.NLSBundle0;
        const errorSeverityHTML = '<i class="fa fa-exclamation-triangle"></i>';
        const warningSeverityHTML = '<i class="fa fa-exclamation-circle"></i>';
        const deprecatedSeverityHTML = '<i class="fa fa-question-circle"></i>';
        this.report.errors.forEach((err) => {
          const row = domConstruct.create('tr', null, this.domNode, 'last');
          domConstruct.create('td', {
            title: messages.error,
            innerHTML: errorSeverityHTML,
          }, row);
          domConstruct.create('td', { innerHTML: err.path }, row);
          domConstruct.create('td', { innerHTML: messages[`report_${err.code}`] }, row);
        }, this);
        this.report.warnings.forEach((warn) => {
          const row = domConstruct.create('tr', null, this.domNode, 'last');
          domConstruct.create('td', {
            title: messages.warning,
            innerHTML: warningSeverityHTML,
          }, row);
          domConstruct.create('td', { innerHTML: warn.path }, row);
          domConstruct.create('td', { innerHTML: messages[`report_${warn.code}`] }, row);
        }, this);

        this.report.deprecated.forEach((dep) => {
          const row = domConstruct.create('tr', null, this.domNode, 'last');
          domConstruct.create('td', {
            title: messages.deprecated,
            innerHTML: deprecatedSeverityHTML,
          }, row);
          domConstruct.create('td', { innerHTML: dep }, row);
          domConstruct.create('td', { innerHTML: messages.deprecated }, row);
        }, this);

        const titleStr = localize(messages, 'reportHead', {
          URI: this.report.uri,
          errors: this.report.errors.length,
          warnings: this.report.warnings.length,
        });
        domAttr.set(this.instanceHeader, 'innerHTML', titleStr);
        if (this.report.errors.length > 0) {
          domClass.add(this.domNode, 'errors');
        } else if (this.report.warnings.length > 0) {
          domClass.add(this.domNode, 'warnings');
        }
      });
    },
    _openView() {
      // var binding = Engine.match(this.graph, this.report.uri, this.template);
      // Engine.report(binding);
      this.validateDialog.title = this.report.uri;
      this.validateDialog.localeChange();
      this.validateDialog.show(this.report.uri, this.graph, this.template);
    },
  });

  return declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    nlsBundles: ['esreReport'],
    templateString: ClassReportTemplate,
    postCreate() {
      this.inherited('postCreate', arguments);
      jquery(this.panel).collapse('hide');
      this.localeReady.then(() => {
        domAttr.set(
          this.headingNode, 'innerHTML',
          localize(this.NLSBundle0, 'instancesHeader', {
            nr: this.reports.length,
            class: ns.shorten(this.rdftype),
          }),
        );
        let nrErr = 0;
        let nrWarn = 0;
        let nrDep = 0;
        this.reports.forEach((rep) => {
          nrErr += rep.errors.length;
          nrWarn += rep.warnings.length;
          nrDep += rep.deprecated.length;
        });
        if (nrErr > 0) {
          domConstruct.create('span', {
            title: localize(this.NLSBundle0, 'errorTitle', nrErr),
            innerHTML: `${nrErr
            }<i class="fa fa-exclamation-triangle"></i>`,
          }, this.problems);
        }
        if (nrWarn > 0) {
          domConstruct.create('span', {
            title: localize(this.NLSBundle0, 'warningTitle', nrWarn),
            innerHTML: `${nrWarn
            }<i class="fa fa-exclamation-circle"></i>`,
          }, this.problems);
        }

        if (nrDep > 0) {
          domConstruct.create('span', {
            title: localize(this.NLSBundle0, 'deprecatedTitle', nrDep),
            innerHTML: `${nrDep
            }<i class="fa fa-question-circle"></i>`,
          }, this.problems);
        }

        // domConstruct.create("h3",
        // {"class": "instanceType", innerHTML: messages.instancesHeader + key}, this._rdformsNode);
        this.reports.forEach((resourceReport) => {
          InstanceReport({
            report: resourceReport,
            validateDialog: this.validateDialog,
            graph: this.graph,
            template: this.template,
          }, domConstruct.create('tbody', {}, this.reportTable, 'last'));
        }, this);
      });
    },
  });
});
