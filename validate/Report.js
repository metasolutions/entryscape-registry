define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/promise/all',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'entryscape-registry/defaults',
  'config',
  './ClassReport',
  'entryscape-commons/rdforms/RDFormsValidateDialog',
  'rdforms/model/validate',
  'di18n/NLSMixin',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dijit/_WidgetsInTemplateMixin',
  'dojo/text!./ReportTemplate.html',
  'i18n!nls/esreReport',
  'i18n!nls/esreSource',
], (
  declare, lang, all, domAttr, domConstruct, defaults, config, ClassReport,
  RDFormsValidateDialog, validate, NLSMixin, _WidgetBase, _TemplatedMixin,
  _WidgetsInTemplateMixin, template,
) => {
  const regroup = (a, p) => {
    const g = {};
    a.forEach((i) => {
      g[i[p]] = g[i[p]] || [];
      g[i[p]].push(i);
    });
    return g;
  };

  const ns = defaults.get('namespaces');
  ns.add('dcat', 'http://www.w3.org/ns/dcat#');
  return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
    //= ==================================================
    // Public attributes
    //= ==================================================
    itemStore: null,
    type2template: null,
    rdfjson: null,
    rdfjsonEditorOpen: false,
    nlsBundles: ['esreReport', 'esreSource'],

    //= ==================================================
    // Inherited attributes
    //= ==================================================
    templateString: template,

    //= ==================================================
    // Inherited methods
    //= ==================================================
    postCreate() {
      this.inherited('postCreate', arguments);
      this._validateDialog = new RDFormsValidateDialog({
        // Placeholder value, just to separate from initial check for empty string
        maxWidth: 800,
      }, this._validateDialogNode);
      this._validateDialog.validator.includeLevel = 'recommended';
      const tp = defaults.onInit('itemstore').then((itemstore) => {
        const t2t = config.registry.type2template;
        this.type2template = {};
        Object.keys(t2t).forEach((cls) => {
          this.type2template[ns.expand(cls)] = itemstore.getItem(t2t[cls]);
        });
        this.mandatoryTypes = config.registry.mandatoryValidationTypes.map(mt => ns.expand(mt));
      });
      this.allInited = all([this.localeReady, tp]);
    },
    show() {
      this._graph = defaults.get('clipboardGraph');
      if (this._graph == null || this._graph.isEmpty()) {
        this.localeReady.then(() => {
          const bundle = this.NLSBundles.esreSource;
          return defaults.get('dialogs').acknowledge(bundle.noRDF, bundle.noRDFProceed);
        }).then(() => {
          defaults.get('siteManager').render('toolkit__rdf__source');
        });
        return;
      }
      this.allInited.then(lang.hitch(this, this._templateBasedValidation));
    },
    //= ==================================================
    // Private methods
    //= ==================================================
    localeChange() {
      this._validateDialog.closeLabel = this.NLSBundles.esreReport.closeValidationDialog;
    },

    _templateBasedValidation() {
      domAttr.set(this._rdformsNode, 'innerHTML', '');
      const report = validate.graphReport(this._graph, this.type2template, this.mandatoryTypes);
      const type2resourceReports = regroup(report.resources, 'type');
      Object.keys(type2resourceReports).forEach((key) => {
        ClassReport({
          rdftype: key,
          reports: type2resourceReports[key],
          validateDialog: this._validateDialog,
          graph: this._graph,
          template: this.type2template[key],
        }, domConstruct.create('div', { class: 'instance' }, this._rdformsNode));
      });
    },
  });
});
