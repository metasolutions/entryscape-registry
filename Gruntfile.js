module.exports = (grunt) => {
  grunt.task.loadTasks('node_modules/entryscape-js/tasks');

  grunt.config.merge({
    poeditor: {
      projectids: ['94871'],
    },
    nls: {
      langs: ['en', 'sv', 'de'],
      depRepositories: ['entryscape-commons', 'entryscape-admin', 'entryscape-catalog', 'entryscape-workbench'],
    },
    update: {
      libs: [
        'di18n',
        'spa',
        'rdfjson',
        'rdforms',
        'store',
        'entryscape-commons',
        'entryscape-admin',
        'entryscape-catalog',
      ],
    },
  });

  grunt.loadNpmTasks('grunt-available-tasks');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
};
