define([
  'dojo/_base/declare',
  'config',
  'entryscape-commons/defaults',
  'entryscape-commons/components/common/Group',
  './components/CardComponent',
  'dojo/dom-class',
  'entryscape-commons/nav/Start',
  'dojo/text!./StartTemplate.html',
], (declare, config, defaults, GroupComponent, CardComponent, domClass, Start) =>
  declare([Start], {
    bid: 'esreStart',

    render() {
      this.renderBanner();
      const site = defaults.get('siteManager');
      const cardsComponents = this.getCards().map((card) => {
        const cardView = this.getCardView(card);
        const text = this.getText(card);
        let onclick;
        const { label, tooltip } = this.getLabelAndTooltip(card);
        if (cardView != null) {
          const viewDef = site.getViewDef(cardView);
          const params = Object.assign({}, this.viewParams || {}, viewDef.showParams || {});
          onclick = () => defaults.get('siteManager').render(cardView, params);
        }

        return m(CardComponent, {
          text,
          tooltip,
          onclick,
          title: label,
          id: this.bid,
          faClass: card.faClass,
        });
      }, this);

      m.render(this.mainNode, m(GroupComponent, { components: cardsComponents }));
    },

    // TODO change in commons so this is not neccessary
    getBannerNode() {
      const node = this.inherited(arguments);
      domClass.remove(node, 'jumbotron');
      domClass.add(node, 'panel panel-default');
      return node;
    },
    canShowView() {
      return new Promise(resolve => resolve(true));
    },
  }));
