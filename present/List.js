define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'entryscape-commons/defaults',
  'entryscape-commons/store/ArrayList',
  './DatasetDialog',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/view/PublicView',
  'i18n!nls/escaDataset',
  'i18n!nls/escoList',
  'i18n!nls/esreSource',
], (
  declare, lang, array, domConstruct, domAttr, defaults, ArrayList, DatasetDialog, BaseList,
  PublicView,
) => {
  const ns = defaults.get('namespaces');

  return declare([BaseList, PublicView], {
    includeCreateButton: false,
    includeInfoButton: true,
    includeEditButton: false,
    includeRemoveButton: false,
    includeHead: false,
    searchVisibleFromStart: false,
    nlsBundles: ['escoList', 'escaDataset', 'esreSource'],
    entryType: ns.expand('dcat:Dataset'),
    class: 'datasets',
    rowClickDialog: 'info',

    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('info', DatasetDialog);
      const listnode = this.getView().domNode;
      this.headerNode = domConstruct.create('h3', { style: { 'margin-bottom': '20px' } }, listnode, 'first');
    },
    localeChange() {
      this.inherited(arguments);
      domAttr.set(this.headerNode, 'innerHTML', this.NLSBundles.esreSource.exploreHeader);
    },
    show() {
      const graph = defaults.get('clipboardGraph');
      if (graph == null || graph.isEmpty()) {
        this.localeReady.then(() => {
          const bundle = this.NLSBundles.esreSource;
          return defaults.get('dialogs').acknowledge(bundle.noRDF, bundle.noRDFProceed);
        }).then(() => {
          defaults.get('siteManager').render('toolkit__rdf__source');
        });
        return;
      }
      this.inherited(arguments);
    },

    showStopSign() {
      return false;
    },
    /**
     *
     * @param params
     * @param row
     * @return {boolean}
     */
    installActionOrNot() {
      return true;
    },
    /**
     *
     * @param entry
     * @return {*}
     */
    getTemplate() {
      if (!this.template) {
        this.template = defaults.get('itemstore').getItem('dcat:OnlyDataset');
      }
      return this.template;
    },
    /**
     *
     * @param params
     */
    search() {
      const graph = defaults.get('clipboardGraph');
      const temp = defaults.get('entrystore').getContextById('__temporary');

      const pelist = array.map(graph.find(null, 'rdf:type', 'dcat:Dataset'), (stmt) => {
        const uri = stmt.getSubject();
        const de = temp.newLink(uri);
        de.setMetadata(graph);
        return de;
      });
      this.listView.showEntryList(new ArrayList({ arr: pelist }));
    },
  });
});
