define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-construct',
  'entryscape-commons/defaults',
  './Dataset',
  'entryscape-commons/dialog/TitleDialog', // In template
], (declare, domAttr, domConstruct, defaults, Dataset, TitleDialog) => declare([TitleDialog], {
  includeFooter: false,
  postCreate() {
    this.inherited(arguments);
    this.dataset = new Dataset({}, domConstruct.create('div', null, this.containerNode));
  },
  open(params) {
    this.entry = params.row.entry;
    this.dataset.graph = defaults.get('clipboardGraph');
    this.dataset.showDataset(params.row.entry);
    domAttr.set(this.titleNode, 'innerHTML', defaults.get('rdfutils').getLabel(this.entry));
    this.show();
  },
}));
