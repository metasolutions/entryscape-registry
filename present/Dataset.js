define([
  'dojo/_base/declare',
  'dojo/Deferred',
  'entryscape-commons/defaults',
  'entryscape-catalog/public/Dataset',
], (declare, Deferred, defaults, Dataset) =>
  declare([Dataset], {
    inDialog: true,
    graph: null,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.distributionInfoDialog.checkForAPI = false;
    },
    /**
     *
     * @param datasetEntry
     * @return {*}
     */
    fetchCatalog() {
      const catalog = this.graph.find(null, 'rdf:type', 'dcat:Catalog')[0].getSubject();
      const temp = defaults.get('entrystore').getContextById('__temporary');
      const ce = temp.newLink(catalog);
      ce.setMetadata(this.graph);
      const d = new Deferred();
      d.resolve(ce);
      return d;
    },

    fetchDistributions(datasetEntry) {
      const md = datasetEntry.getMetadata();
      const temp = defaults.get('entrystore').getContextById('__temporary');
      const stmts = md.find(datasetEntry.getResourceURI(), 'dcat:distribution');
      const dists = stmts.map((stmt) => {
        const diURI = stmt.getValue();
        const di = temp.newLink(diURI);
        di.setMetadata(this.graph);
        return di;
      }, this);
      const d = new Deferred();
      d.resolve(dists);
      return d;
    },
  }));
