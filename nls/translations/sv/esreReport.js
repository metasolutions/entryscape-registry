define({
  "validationReport": "Valideringsrapport för katalogen",
  "instancesHeader": "{{PLURAL:${nr}|En instans|${nr} instances}} till klassen ${class}:",
  "severity": "Hur allvarligt",
  "path": "Egenskap i grafen",
  "problem": "Problem",
  "error": "Fel",
  "errorTitle": "{{PLURAL:$1|1 fel|$1 fel}}",
  "warning": "Varning",
  "warningTitle": "{{PLURAL:$1|1 varning|$1 varningar}}",
  "deprecated": "Föråldrat värde",
  "deprecatedTitle": "{{PLURAL:$1|1 föråldrat värde|$1 föråldrade värden}}",
  "report_few": "För få värden",
  "report_many": "För många värden",
  "report_disjoint": "Endast ett värde tillåtet",
  "reportHead": "${URI} har {{PLURAL:${errors}|ett fel|${errors} fel}} och {{PLURAL:${warnings}|en varning|${warnings} varningar}}:",
  "closeValidationDialog": "Stäng",
  "viewValidation": "Visa valideringsresultat"
});