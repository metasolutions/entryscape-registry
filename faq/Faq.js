define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'entryscape-workbench/defaults',
  'entryscape-workbench/bench/Bench',
], (declare, lang, defaults, Bench) => declare([Bench], {
  show(params) {
    params.context = 'faq';
    const es = defaults.get('entrystore');
    const context = es.getContextById('faq');
    defaults.set('context', context);
    this.inherited(arguments);
    this.checkIfCreated(context);
  },
  checkIfCreated(context) {
    if (defaults.get('isAdmin')) {
      context.getEntry().then(null, lang.hitch(this, () => {
        console.log('No FAQ context, creating it');
        const es = defaults.get('entrystore');
        const nge = es.newGroup('faq', 'faq');
        nge.getMetadata().addL(nge.getResourceURI(), 'foaf:name', 'FAQ group');
        nge.commit().then((groupEntry) => {
          const nce = es.newContext('faq', 'faq');
          const cei = nce.getEntryInfo();
          const etype = es.getResourceURI('entitytypes', 'question');
          const ruri = nce.getResourceURI();
          const eigraph = cei.getGraph();
          eigraph.add(ruri, 'esterms:entityType', etype);
          eigraph.add(ruri, 'rdf:type', 'esterms:WorkbenchContext');

          const acl = cei.getACL(true);
          acl.rread.push('_guest');
          acl.mread.push('_guest');
          acl.mwrite.push(groupEntry.getId());
          acl.rwrite.push(groupEntry.getId());
          cei.setACL(acl);
          nce.getMetadata().addL(nge.getResourceURI(), 'dcterms:title', 'FAQ project');
          nce.commit().then((contextEntry) => {
            const groupResource = groupEntry.getResource(true);
            groupResource.setHomeContext(contextEntry.getId());
          });
        });
      }));
    }
  },
}));
