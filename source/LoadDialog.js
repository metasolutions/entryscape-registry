define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/dom-construct',
  '../defaults',
  'entryscape-commons/dialog/TitleDialog',
  'entryscape-commons/create/EntryType',
  'rdfjson/formats/converters',
], (declare, lang, domConstruct, defaults, TitleDialog, EntryType, converters) =>
  declare([TitleDialog], {
    explicitNLS: true,
    bundle: null,
    postCreate() {
      this.inherited(arguments);
      this.entryType = new EntryType({
        valueChange: (value) => {
          if (value != null) {
            this.unlockFooterButton();
          } else {
            this.lockFooterButton();
          }
        },
      }, domConstruct.create('div', null, this.containerNode));
    },
    show(callback, bundle) {
      this.entryType.show(true, true, false);
      this.inherited(arguments);
      this.callback = callback;
      this.updateLocaleStringsExplicit(
        bundle.loadTitle,
        bundle.loadButton, bundle.loadButtonTitle,
      );
    },
    footerButtonAction() {
      const cb = this.callback;
      const val = this.entryType.getValue();
      const f = (data) => {
        const report = converters.detect(data);
        if (!report.error) {
          cb(report.graph, val);
        } else {
          throw report.error;
        }
      };

      if (this.entryType.isFile()) {
        const inp = this.entryType.getFileInputElement();
        return defaults.get('entrystore').echoFile(inp, 'text').then(f);
      }
      return defaults.get('entrystore').loadViaProxy(val, 'application/rdf+xml').then(f);
    },
  }));
