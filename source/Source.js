define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/promise/all',
  'dojo/dom-attr',
  'dojo/dom-style',
  '../defaults',
  'config',
  'rdfjson/formats/converters',
  'entryscape-commons/rdforms/RDFEdit',
  'entryscape-commons/rdforms/RDFormsValidateDialog',
  'rdforms/model/validate',
  'rdforms/view/renderingContext',
  'di18n/NLSMixin',
  'di18n/localize',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./SourceTemplate.html',
  'dojo/on',
  './example',
  './LoadDialog',
  '../shim/saveAs',
  'i18n!nls/esreLoadDialog',
  'i18n!nls/esreSource',
], (
  declare, lang, all, domAttr, domStyle, defaults, config, converters, RDFEdit,
  RDFormsValidateDialog, validate, renderingContext, NLSMixin, localize, _WidgetBase,
  _TemplatedMixin, template, on, exampleRDF, LoadDialog,
) => {
  const regroup = (a, p) => {
    const g = {};
    a.forEach((i) => {
      g[i[p]] = g[i[p]] || [];
      g[i[p]].push(i);
    });
    return g;
  };

  let doWarn = true;

  const ns = defaults.get('namespaces');
  ns.add('dcat', 'http://www.w3.org/ns/dcat#');
  return declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    //= ==================================================
    // Public attributes
    //= ==================================================
    bid: 'esreSource',
    itemStore: null,
    type2template: null,
    rdfjson: null,
    rdfjsonEditorOpen: false,
    nlsBundles: ['esreSource', 'esreLoadDialog'],

    //= ==================================================
    // Inherited attributes
    //= ==================================================
    templateString: template,

    //= ==================================================
    // Inherited methods
    //= ==================================================
    postCreate() {
      this.inherited('postCreate', arguments);

      this.loadDialog = new LoadDialog({}, this.loadDialogNode);
      // on(this.uploadButton, "click", lang.hitch(this.uploadDialog, "show"));

      this._rdfEditor = new RDFEdit({
        onRDFChange: lang.hitch(this, this._onRDFChange),
      }, this._rdfEditor);
      const tp = defaults.onInit('itemstore').then((itemstore) => {
        const t2t = config.registry.type2template;
        this.type2template = {};
        Object.keys(t2t).forEach((cls) => {
          this.type2template[ns.expand(cls)] = itemstore.getItem(t2t[cls]);
        });
        this.mandatoryTypes = config.registry.mandatoryValidationTypes.map(mt => ns.expand(mt));
      });
      this.allInited = all([this.localeReady, tp]);
    },
    show() {
      // TODO @scazan: verify with Matthias this.NLSBundles
      this.allInited.then(() => {
        if (defaults.get('userInfo').id === '_guest' && doWarn) {
          const registrySourceBundle = this.NLSBundles.esreSource;
          defaults.get('dialogs').acknowledge(registrySourceBundle.signinRequirement, registrySourceBundle.signinRequirementOk);
          doWarn = false;
        }
        this._rdfEditor.startup();
        const graph = defaults.get('clipboardGraph');
        this._rdfEditor.setRDF(graph || '<?xml version="1.0"?>\n' +
          '<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">\n' +
          '</rdf:RDF>');
        this._onRDFChange();
      });
    },

    showRDF(rdf) {
      const report = this._rdfEditor.setRDF(rdf);
      defaults.set('clipboardGraph', report.graph);
      this._onRDFChange();
    },
    //= ==================================================
    // Private methods
    //= ==================================================
    _onRDFChange() {
      const rdfReport = this._rdfEditor.getRDF();
      if (rdfReport.error) {
        this.viewReportState = false;
        this.updateMessage(rdfReport.error);
        return;
      }
      this.viewReportState = true;
      defaults.set('clipboardGraph', rdfReport.graph);

      const validateReport =
        validate.graphReport(rdfReport.graph, this.type2template, this.mandatoryTypes);
      regroup(validateReport.resources, 'type');
      const validationResult = localize(this.NLSBundles.esreSource, 'validationMessage', {
        errors: validateReport.errors,
        warnings: validateReport.warnings,
      });
      if (validateReport.mandatoryError) {
        this.updateMessage(`${this.NLSBundles.esreSource.mandatoryMissing
        }<ul><li>${validateReport.mandatoryError.join('</li><li>')}</li></ul>`, validationResult);
      } else {
        this.updateMessage(null, validationResult);
      }
    },
    updateMessage(error, success) {
      this.message = [];
      if (error != null) {
        domStyle.set(this.__error, 'display', '');
        domStyle.set(this.__info, 'display', 'none');
        this.message.push(error);
        domAttr.set(this.downloadButton, 'disabled', 'disabled');
      } else {
        domStyle.set(this.__error, 'display', 'none');
        domStyle.set(this.__info, 'display', '');
        domAttr.remove(this.downloadButton, 'disabled');
      }
      if (success) {
        this.message.push(success);
      }
    },
    _infoClick() {
      defaults.get('dialogs').acknowledge(this.message.join('<br>'));
    },
    _example() {
      this.showRDF(exampleRDF);
    },
    _download() {
      const graph = defaults.get('clipboardGraph');
      const rdfxml = converters.rdfjson2rdfxml(graph);
      const blob = new Blob([rdfxml], { type: 'application/rdf+xml;charset=utf-8' });
      saveAs(blob, 'MergedCatalog.rdf', true);
    },
    _upload() {
      if (defaults.get('userInfo').id === '_guest') {
        const b = this.NLSBundles.esreSource;
        defaults.get('dialogs').acknowledge(b.signinRequirement, b.signinRequirementOk);
      } else {
        this.loadDialog.show(
          lang.hitch(this, this.showRDF),
          this.NLSBundles.esreLoadDialog,
        );
      }
    },
  });
});
