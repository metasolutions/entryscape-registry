define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'entryscape-commons/list/EntryRow',
  'dojo/text!./HarvestingRowTemplate.html',
], (declare, domAttr, EntryRow, template) => {
  const checkStr = "<i data-dojo-attach-point='statusNode' class='fa fa-check fa-lg'></i>";

  return declare([EntryRow], {
    templateString: template,
    entry: null,
    showCol1: true,
    showCol3: true,
    rowButtonMenu: null,
    nlsDateTitle: 'creationDateTitle',

    /**
     * @param generic
     * @param specific
     */
    updateLocaleStrings() {
      this.inherited(arguments);
    },
    render() {
      this.inherited(arguments);
      const md = this.entry.getMetadata();
      if (md.findFirstValue(null, 'storepr:check')) {
        domAttr.set(this.psiPage, 'innerHTML', checkStr);
      }
      if (md.findFirstValue(null, 'storepr:merge')) {
        domAttr.set(this.dcatAP, 'innerHTML', checkStr);
      }
      this.renderDate();
    },
  });
});
