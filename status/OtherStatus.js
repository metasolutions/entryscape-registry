define([
  'dojo/_base/declare',
  'entryscape-commons/defaults',
  'store/types',
  './HarvestingRow',
  'entryscape-commons/list/common/BaseList',
  'entryscape-commons/view/PublicView',
  '../harvest/PipelineResultsViewDialog',
  'i18n!nls/escoList',
  'i18n!nls/esreStatus',
], (declare, defaults, types, HarvestingRow, BaseList, PublicView, PipelineResultsViewDialog) =>
  declare([BaseList, PublicView], {
    nlsBundles: ['escoList', 'esreStatus'],
    includeCreateButton: false,
    includeEditButton: false,
    includeInfoButton: false,
    includeHead: true,
    includeSortOptions: false,
    limit: 20,
    class: 'otherOrg',
    rowClass: HarvestingRow,
    rowClickDialog: 'pipelineResultRowAction',
    useNoLangSort: true,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.registerDialog('pipelineResultRowAction', PipelineResultsViewDialog);
    },

    /**
     * @param generic
     * @param specific
     */
    updateLocaleStrings() {
      this.inherited(arguments);
      const b = this.NLSBundles.esreStatus;
      this.getView().setTableHead(`${"<tr class='psirow'>" +
        "<th class='vmiddle entryName'>"}${b.otherOrgLabel}</th>` +
        `<th class='vmiddle dcatAP'>${b.dcatAPLabel}</th>` +
        `<th class='vmiddle harvestDate'>${b.checkedLabel}</th>` +
        '</tr>');
    },

    showStopSign() {
      return false;
    },

    getSearchObject() {
      /** @type {store/EntryStore} */
      const es = defaults.get('entrystore');
      return es.newSolrQuery()
        .graphType(types.GT_PIPELINERESULT)
        .limit(this.limit)
        .tagLiteral(['latest'])
        .literalProperty('dcterms:subject', 'psi', 'not');
    },
  }));
