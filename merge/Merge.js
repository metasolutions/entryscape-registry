define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/dom-attr',
  'dojo/dom-construct',
  './LoadDialog',
  './CatalogDetect',
  './mergeScript',
  'entryscape-commons/defaults',
  'entryscape-commons/view/PublicView',
  'di18n/NLSMixin',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./MergeTemplate.html',
  'i18n!nls/esreMerge',
  'i18n!nls/esreSource',
], (
  declare, lang, array, domAttr, domConstruct, LoadDialog, CatalogDetect, merge, defaults,
  PublicView, NLSMixin, _WidgetBase, _TemplatedMixin, template,
) =>
  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit, PublicView], {
    bid: 'esreMerge',
    nlsBundles: ['esreMerge', 'esreSource'],
    templateString: template,
    __mainCatalog: null,
    __mergeCatalogList: null,
    __mergeCatalogsIntoMain: null,

    postCreate() {
      this.inherited('postCreate', arguments);
      this.mergeCatalogsList = [];
      this.loadDialog = new LoadDialog({ merge: this });
    },

    updateMainCatalog(rdf, source) {
      if (this.mainCatalog) {
        this.mainCatalog.remove();
        delete this.mainCatalog;
      }

      this.mainCatalog = new CatalogDetect(
        { isSlim: true, rdf, source },
        domConstruct.create('div', null, this.__mainCatalog),
      );
      this.updateMerge();
    },

    addMergeCatalog() {
      if (defaults.get('userInfo').id === '_guest') {
        const b = this.NLSBundles.esreSource;
        defaults.get('dialogs').acknowledge(b.signinRequirement, b.signinRequirementOk);
      } else {
        this.loadDialog.show(lang.hitch(this, this.newMergeCatalog));
      }
    },

    newMergeCatalog(rdf, source) {
      this.mergeCatalogsList.push(new CatalogDetect({
        mergeCheck: true,
        rdf,
        source,
        removeCallback: (cd) => {
          this.mergeCatalogsList.splice(this.mergeCatalogsList.indexOf(cd), 1);
          this.updateMerge();
        },
      }, domConstruct.create('li', null, this.__mergeCatalogList)));
      this.updateMerge();
    },

    readyToMerge() {
      return this.mainCatalog && this.mainCatalog.error == null
        && array.some(this.mergeCatalogsList, mc => mc.error == null);
    },

    updateMerge() {
      if (this.readyToMerge()) {
        domAttr.remove(this.__mergeCatalogsIntoMain, 'disabled');
      } else {
        domAttr.set(this.__mergeCatalogsIntoMain, 'disabled', 'disabled');
      }
    },
    mergeCatalogsIntoMain() {
      if (this.readyToMerge()) {
        const mrdflist = this.mergeCatalogsList.filter(cl => cl.error === null).map(cl => cl.rdf);
        merge(this.mainCatalog.rdf, mrdflist);
        this.updateMainCatalog(this.mainCatalog.rdf, 'Main catalog');
        this.clearMergeCatalogs();
      }
    },
    show() {
      const graph = defaults.get('clipboardGraph');
      if (graph == null || graph.isEmpty()) {
        this.localeReady.then(() => {
          const bundle = this.NLSBundles.esreSource;
          return defaults.get('dialogs').acknowledge(bundle.noRDF, bundle.noRDFProceed);
        }).then(() => {
          defaults.get('siteManager').render('toolkit__rdf__source');
        });
        return;
      }

      this.updateMainCatalog(graph, 'Main catalog');
    },
    clearMergeCatalogs() {
      array.forEach(this.mergeCatalogsList.slice(), (cl) => {
        cl.destroy();
      });
      this.__mergeCatalogList = [];
    },
  }));
