define([
  'dojo/_base/declare',
  'dojo/dom-attr',
  'dojo/dom-class',
  'dojo/dom-construct',
  '../defaults',
  'di18n/NLSMixin',
  'di18n/localize',
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  'dojo/text!./CatalogDetectTemplate.html',
  'i18n!nls/esreCatalogDetect',
], (
  declare, domAttr, domClass, domConstruct, defaults, NLSMixin, localize, _WidgetBase, _TemplatedMixin,
  template,
) =>
  declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
    bid: 'esreCatalogDetect',
    nlsBundles: ['esreCatalogDetect'],
    templateString: template,
    mergeCheck: false,
    rdf: null,
    error: null,
    source: '',
    isSlim: false,

    __catalogStatus: null,
    __catalogSource: null,

    postCreate() {
      this.inherited(arguments);
      if (this.isSlim) {
        domClass.add(this.domNode, 'esreCatalogDetect--slim');
      }
    },

    localeChange() {
      let ca = 0;
      let da = 0;
      let di = 0;
      let co = 0;
      let pu = 0;
      (this.rdf.find(null, 'rdf:type') || []).forEach((stmt) => {
        switch (stmt.getValue()) {
          case 'http://www.w3.org/ns/dcat#Catalog':
            ca += 1;
            break;
          case 'http://www.w3.org/ns/dcat#Dataset':
            da += 1;
            break;
          case 'http://www.w3.org/ns/dcat#Distribution':
            di += 1;
            break;
          case 'http://xmlns.com/foaf/0.1/Agent':
          case 'http://xmlns.com/foaf/0.1/Person':
          case 'http://xmlns.com/foaf/0.1/Organization':
            co += 1;
            break;
          case 'http://www.w3.org/2006/vcard/ns#Kind':
          case 'http://www.w3.org/2006/vcard/ns#Individual':
          case 'http://www.w3.org/2006/vcard/ns#Organization':
            pu += 1;
            break;
          default:
            break;
        }
      });
      let text;
      let hasError;
      const b = this.NLSBundle0;
      const iconStatus = hasError ? { class: 'fa fa-exclamation-triangle' } : { class: 'fa fa-check-circle' };

      if (ca > 1) {
        hasError = true;
        text = localize(b, 'catalogError', ca);
        this.error = text;
      } else if (this.mergeCheck && da === 0 && di === 0 && co === 0 && pu === 0) {
        hasError = true;
        text = b.nothingToMergeError;
        this.error = text;
      } else {
        hasError = false;
        text = localize(b, 'catalogStatus', {
          da, di, co, pu,
        });
      }
      domConstruct.create('i', iconStatus, this.__catalogStatus);
      domConstruct.create('p', { innerHTML: text }, this.__catalogStatus);

      domAttr.set(this.__catalogSource, 'innerHTML', this.source);
    },
    remove() {
      if (this.removeCallback) {
        this.removeCallback(this);
      }
      this.destroy();
    },
  }));
