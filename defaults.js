define([
  'entryscape-catalog/defaults',
  'config',
  'entryscape-commons/create/typeIndex',
], (defaults, config, typeIndex) => {
  const ns = defaults.get('namespaces');
  ns.add('storepr', 'http://entrystore.org/terms/pipelineresult#');

  const mandatoryTypes = config.registry.mandatoryValidationTypes.map(mt => ns.expand(mt));
  defaults.set('mandatoryValidationTypes', mandatoryTypes);

  // Copy over templates from entityTypes (and indirectly from catalog config)
  if (!config.registry.type2template && config.registry.validationTypes) {
    const constraints = {};
    const rtype = ns.expand('rdf:type');
    config.registry.type2template = {};
    const t2t = config.registry.type2template;

    config.registry.validationTypes.forEach((vt) => {
      constraints[rtype] = ns.expand(vt);
      const conf = typeIndex.getConfFromConstraints(constraints);
      t2t[vt] = conf.template;
    });
  }

  const validationType2template = {};
  defaults.onInit('itemstore').then((itemstore) => {
    const t2t = config.registry.type2template;
    Object.keys(t2t).forEach((cls) => {
      validationType2template[ns.expand(cls)] = itemstore.getItem(t2t[cls]);
    });
  });

  defaults.set('type2template', validationType2template);
  return defaults;
});
