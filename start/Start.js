define([
  'dojo/_base/declare',
  'config',
  'entryscape-commons/defaults',
  'dojo/dom-class',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'entryscape-commons/nav/Start',
  'entryscape-commons/view/PublicView',
  'dojo/text!./StartTemplate.html',
], (declare, config, defaults, domClass, domConstruct, domAttr, Start, PublicView) =>
  declare([Start, PublicView], {
    bid: 'esreStart',

    render() {
      domAttr.set(this.mainNode, 'innerHTML', '');
      const site = defaults.get('siteManager');
      this.renderBanner();
      const cards = this.getCards();
      cards.forEach((card) => {
        const colOuter = domConstruct.create('div', {
          class: 'col-xs-12 col-md-6',
          style: 'padding: 0 7.5px',
        }, this.mainNode);
        const colInner = domConstruct.create('div', { class: `col-xs-12 ${this.bid}__card panel` }, colOuter);
        const a = domConstruct.create('a', {
          class: (card.view == null ? 'disabled' : ''),
        }, colInner);
        if (card.view != null) {
          const viewDef = site.getViewDef(card.view);
          const params = Object.assign(({}, this.viewParams || {}), (viewDef.showParams || {}));
          domAttr.set(a, 'href', site.getViewPath(card.view, params));
        }
        domConstruct.create('i', { class: `${this.bid}__cardIcon pull-right fa fa-2x fa-${card.faClass}` }, a);
        const titleNode = domConstruct.create('h3', { class: `${this.bid}__cardHeader` }, a);
        const textNode = domConstruct.create('p', { class: `${this.bid}__cardParagraph` }, a);
        this.setLabelAndTooltip(titleNode, a, card);
        this.setText(textNode, card);
      }, this);
    },

    // TODO change in commons so this is not neccessary
    getBannerNode() {
      const node = this.inherited(arguments);
      domClass.remove(node, 'jumbotron');
      domClass.add(node, 'panel panel-default');
      return node;
    },
  }));
